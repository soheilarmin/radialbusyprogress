import QtQuick 2.15

Item {

    id: root

    property real progress: 0
    property int duration: 2500
    property int entities: 25
    property alias textItem: _textItem.sourceComponent

    property color color1:  '#dddddd'
    property color color2:  '#46fa32'

    readonly property real radius: Math.min(width,height) / 2
    readonly property real perimeter: 2 * Math.PI * radius
    readonly property alias currentEntityIndex: repeater.currentEntity
    readonly property real stepDuration: root.duration / root.entities

    Loader{
        id: _textItem
        anchors.fill: parent
        sourceComponent : Text {
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter; horizontalAlignment: Text.AlignHCenter
            text: "%1%".arg(Math.round(root.progress))
            font.pixelSize: parent ? parent.height/ 6 : 14
        }
    }

    Repeater {
        id: repeater
        model: root.entities
        anchors.centerIn: parent.Center
        property int currentEntity : 0
        delegate:  Rectangle {
            id: entity

            property real _rotation: (-360 / root.entities) * index
            property alias xScale: scaleTransform.xScale
            property bool filled: ((index+1) / entities) * 100 <= root.progress

            anchors.centerIn: parent
            width: root.radius / 5
            height:  root.perimeter / root.entities / 2
            radius: height/2

            transformOrigin: Item.Center

            color:  filled ? root.color2 : root.color1
            opacity: scaleTransform2.xScale

            transform: [
                Scale{
                    id: scaleTransform
                    origin.x: 0
                    origin.y: height/2
                    xScale: 0.2
                },
                //second transform to supress the previous one if the entity is filled, preseving a slight animation
                Scale{
                    id: scaleTransform2
                    origin.x: 0
                    origin.y: height/2
                    xScale: filled ?  (1 / scaleTransform.xScale) * (Math.pow(scaleTransform.xScale, 0.1)) : 1;
                },
                Rotation{
                    angle: _rotation
                    origin.x: width/2
                    origin.y: height/2
                },
                Translate{
                    x: Math.cos(_rotation * Math.PI / 180.0) * (root.radius - width/2);
                    y: Math.sin(_rotation * Math.PI / 180.0) * (root.radius - width/2);
                }
            ]

            SequentialAnimation{
                id: anim
                loops: Animation.Infinite

                ScriptAction{ script: repeater.currentEntity = index }

                PropertyAnimation{
                    target: entity
                    property: 'xScale'
                    to: 1
                    duration: (root.entities) * root.stepDuration * 1 / 15
                }

                PropertyAnimation{
                    target: entity
                    property: 'xScale'
                    to: 0.4
                    duration: (root.entities) * root.stepDuration * 7 / 15
                }

                PauseAnimation { duration: (root.entities) * root.stepDuration * 2 / 15 }

                PropertyAnimation{
                    target: entity
                    property: 'xScale'
                    to: 0.2
                    duration: (root.entities) * root.stepDuration * 2 / 15
                }

                PauseAnimation { duration: (root.entities) * root.stepDuration * 3 / 15 }
            }

            Timer{
                id: startTimer
                interval: index * root.stepDuration
                running: true
                repeat: false
                onTriggered: anim.start()
            }

            Behavior on color{ColorAnimation{duration: root.stepDuration}}
        }
    }
}
