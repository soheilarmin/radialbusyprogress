import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("RadialBusyProgress Demo ")

    Timer{
        property real val: 0
        repeat: true
        interval: 100
        running: true
        onTriggered: layout.progress = val < 100 ? val++ : val=0
    }

    GridLayout{
        id: layout
        property real progress: 0
        columns: 3
        anchors.fill: parent

        RadialBusyProgress{
            id: p1
            Layout.fillWidth: true
            Layout.fillHeight: true
            progress: layout.progress
        }

        RadialBusyProgress{
            id: p2
            Layout.fillWidth: true
            Layout.fillHeight: true

            color1: 'purple'

            textItem: Text{
                text: p2.currentEntityIndex
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                rotation: (-360/ p2.entities ) * p2.currentEntityIndex

                Rectangle{
                    width: p2.radius *2
                    height:  p2.radius / 6
                    anchors.centerIn: parent
                    color: 'red'
                    opacity: 0.1
                }
                Behavior on rotation {
                    RotationAnimation { duration: 100; direction: RotationAnimation.Shortest }
                }
            }

        }

        RadialBusyProgress{
            id: p3
            Layout.fillWidth: true
            Layout.fillHeight: true
            progress: layout.progress


        }

        RadialBusyProgress{
            id: p4
            entities: 16
            Layout.fillWidth: true
            Layout.fillHeight: true
            progress: layout.progress
        }

        RadialBusyProgress{
            id: p5
            entities: 12
            Layout.fillWidth: true
            Layout.fillHeight: true
            progress: layout.progress
            duration: 1000

        }

        RadialBusyProgress{
            id: p6
            entities: 48
            Layout.fillWidth: true
            Layout.fillHeight: true
            progress: layout.progress
        }
    }
}
