# Radial Busy Progress (QML implementation)

Inspired from https://github.com/pooyasis/Qml-Progressbar

![Demo Video](media/demo.mp4)

## Usage:

Add ``RadialBusyProgress.qml`` to your project and use it like any other QML file.

### Properties:


| Property|||
| ------|:----| -----------:|
|progress| Current progress value [0,100] |
|duration| Duration of the animation |
|entities| Number of entities|
|textItem| In type of ```Item``` to customize the inner text item. The default values shows the progress value.|
|color1|Color of entities when *not* filled by the progress value|
|color2|Color of entities when *filled* by the progress value|
|radius|Radius of the radial progress (Minimum of width/2 and height/2)|*readonly*
|currentEntityIndex|Current entity that an individual animation is starting on it|*readonly*



